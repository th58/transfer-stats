package main

import (
    "os"
    "fmt"
    "math"
    "sort"
    "errors"
    "strings"
    "io/ioutil"
    "encoding/json"
    "strconv"
    "time"
)

type JsonRoot struct {
    Object []User `json:"object"`
}

type User struct {
    Id Oid `json:"_id"`
    V int `json:"__v"`
    Courses []Course `json:"courses"`
    Created CreationDate `json:"created"`
    Email string `json:"email"`
    Token string `json:"token"`
}

type Oid struct {
    Id string `json:"$oid"`
}

type Course struct {
    Name string `json:"course"`
    Organization string `json:"organization"`
    Term string `json:"term"`
    Year string `json:"year"`
    Index string `json:"index"`
}

type CreationDate struct {
    Date JSONTime `json:"$date"`
}

type JSONTime time.Time

func (t *JSONTime) UnmarshalJSON(b []byte) error {
    dateString := strings.Replace(string(b), "\"", "", -1)
    result, err := time.Parse("2006-01-02T15:04:05.000-0700", dateString)
    if err != nil {
        return errors.New(fmt.Sprintf("Error parsing date: %s, Error content: %v", string(b), err))
    }
    *t = JSONTime(result)
    return nil
}

func (t JSONTime) String() string {
    return time.Time(t).Format("2006-01-02T15:04:05.000-0700")
}

func getMin(s []float64) float64 {
    var min float64 = 999.0
    for i:=0; i<len(s); i++ {
        if s[i] < min {
            min = s[i]
        }
    }
    return min
}

func getMax(s []float64) float64 {
    var max float64 = 0.0
    for i:=0; i<len(s); i++ {
        if s[i] > max {
            max = s[i]
        }
    }
    return max
}

func getMedian(s []float64) float64 {
    var n int = len(s)
    if n % 2 != 1 {
        return (s[n / 2] + s[n / 2 - 1]) / 2
    }
    return s[n / 2]
}

func getQ1(s []float64) float64 {
    var n int = len(s)
    if n % 2 != 1 {
        return getMedian(s[:n/2])
    }
    return getMedian(s[:n/2])
}

func getQ3(s []float64) float64 {
    var n int = len(s)
    if n % 2 != 1 {
        return getMedian(s[n/2:])
    }
    return getMedian(s[n/2+1:])
}

func getMean(s []float64) float64 {
    var sum float64 = 0.0
    var n int = len(s)
    for i:=0; i<n; i++ {
        sum += s[i]
    }
    mean := sum/float64(n)
    return mean
}

func getVariance(s []float64, mean float64) float64 {
    var squares []float64
    for _, value := range s {
        distToMean := value - mean
        squares = append(squares, distToMean * distToMean)
    }
    return getMean(squares)
}

func getStdDev(variance float64) float64 {
    return math.Sqrt(variance)
}

func main() {

    // Get name of json file to use
    if len(os.Args) < 2 {
        fmt.Println("Usage: transfer-stats target.json")
        return
    }
    fileName := os.Args[1]
    fmt.Printf("Reading file: %s\n", fileName)

    file, err := ioutil.ReadFile(fileName)
    if err != nil {
        fmt.Println("%s\n", err)
        return
    }

    var vals JsonRoot
    err = json.Unmarshal(file, &vals)
    if err != nil {
        fmt.Printf("%s\n", err)
        return
    }

    count := 0
    var courseCounts []float64
    creationCounts := make(map[string]int)
    for _,v := range vals.Object {
        count++
        courseCounts = append(courseCounts, float64(len(v.Courses)))
        y, m, d := time.Time(v.Created.Date).Date()
        dateString := strconv.Itoa(y) + "-" + strconv.Itoa(int(m)) + "-" + strconv.Itoa(d)
        i, ok := creationCounts[dateString]
        if ok {
            creationCounts[dateString] = i + 1
        } else {
            creationCounts[dateString] = 1
        }
        fmt.Printf("%s\n", dateString)
    }
    // fmt.Printf("creation counts: %v\n", creationCounts)

    var allCreationCounts []float64
    for _,v := range creationCounts {
        allCreationCounts = append(allCreationCounts, float64(v))
    }
    sort.Float64s(allCreationCounts)

    allCreationCountsZeros := make([]float64, len(allCreationCounts))
    copy(allCreationCountsZeros, allCreationCounts)

    for i:=0; i<37; i++ {
        allCreationCountsZeros = append(allCreationCountsZeros, 0.0)
    }
    sort.Float64s(allCreationCountsZeros)

    fmt.Printf("Creation days: %v\n", allCreationCounts)
    fmt.Println("Number of days: " + strconv.Itoa(len(allCreationCounts)))
    fmt.Printf("Mean creations per day: %f\n", getMean(allCreationCounts))
    fmt.Printf("Median creations per day: %f\n", getMedian(allCreationCounts))
    fmt.Printf("Max creations per day: %f\n", getMax(allCreationCounts))
    fmt.Printf("Min creations per day: %f\n", getMin(allCreationCounts))
    fmt.Printf("Std Dev creations per day: %f\n", getStdDev(getVariance(allCreationCounts, getMean(allCreationCounts))))

    fmt.Printf("Creation days (zeros): %v\n", allCreationCountsZeros)
    fmt.Printf("Mean creations per day (zeros): %f\n", getMean(allCreationCountsZeros))
    fmt.Printf("Median creations per day (zeros): %f\n", getMedian(allCreationCountsZeros))
    fmt.Printf("Std Dev creations per day (zeros): %f\n", getStdDev(getVariance(allCreationCountsZeros, getMean(allCreationCountsZeros))))

    /*
    fmt.Println("Number of entries: " + strconv.Itoa(count))
    fmt.Printf("Course Counts: %v\n", courseCounts)
    fmt.Printf("Mean Course Count: %f\n", getMean(courseCounts))
    fmt.Printf("Median Course Count: %f\n", getMedian(courseCounts))
    fmt.Printf("Max Course Count: %f\n", getMax(courseCounts))
    fmt.Printf("Min Course Count: %f\n", getMin(courseCounts))
    fmt.Printf("Variance Course Count: %f\n", getVariance(courseCounts, getMean(courseCounts)))
    fmt.Printf("Std Dev Course Count: %f\n", getStdDev(getVariance(courseCounts, getMean(courseCounts))))
    */
}

